# Background

From an initial analysis of GitHub API usage, it was learnt that the project boards api is relatively underused. 
Octokit libraries don't support projects api yet. Even GitHub API requires a special preview header for usage.
For a desire of originality, this was chosen as the goal of this project - to provide a simple dashboard of an Organization's
projects. This dashboard would consist of two different pieces - 
1. A list of all commits made to the organization's repos within a given period of time grouped by author and then sub-grouped by repository
2. A list of projects owned by the organization and their status (%'s of different categories of issues on those respective project boards)

### First Goal
The first goal was achieved with the UI as well as the backend working together to displaying such a list.

![picture](/readme-images/commits.png)

![picture](/readme-images/commits-api.png)

### Second goal
The second goal was half-achieved by the time of submission - server portion has been completed but UI work needs to be done.

![picture](/readme-images/projectstatus-swagger.png)

# Project setup

This application was developed as two completely independent components - client and server - following the popular enterprise development pattern.

In the project directory, respective folders could be found for each one of the above components

### Client 

Client is a simple Vue app. To run the app locally, navigate to client directory and run "npm run serve" and the UI will be accessible through http://localhost:8080

### Server

Server is a .NET core 2.2 based web api app. To run the app locally, navigate to server directory and run "dotnet run" and a Swagger page should open in browser with API documentation and options to try it out.

# Pending Issues
1. State issue with UI - due to the author's relatively basic understanding of Vue's state management, this issue couldn't be solved in time for submission of this assessment
2. UX clean up
3. Client - Server authentication
4. Using GitHub Oauth integration instead of personal key usage
5. Docker-ization
