﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using server.Exceptions;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace server.Connectors
{
    public class BaseHttpClient : IBaseHttpClient, IDisposable
    {

        private readonly HttpClient _httpClient;
        private readonly ILogger _logger;

        public BaseHttpClient(ILogger<BaseHttpClient> logger)
        {
            _httpClient = new HttpClient();
            _logger = logger;
        }

        public async Task<T> Get<T>(string url, Dictionary<string, string> headers) where T : class
        {
            var result = await MakeHttpCall<T>(HttpMethod.Get, url, headers, null);
            return result;
        }

        public async Task<T> Post<T>(string url, Dictionary<string, string> headers, object request) where T:class
        {
            var result = await MakeHttpCall<T>(HttpMethod.Post, url, headers, request);
            return result;
        }

        public async Task<T> Put<T>(string url, Dictionary<string, string> headers, object request) where T : class
        {
            var result = await MakeHttpCall<T>(HttpMethod.Put, url, headers, request);
            return result;
        }

        public async Task<T> Delete<T>(string url, Dictionary<string, string> headers) where T : class
        {
            var result = await MakeHttpCall<T>(HttpMethod.Delete, url, headers, null);
            return result;
        }

        private async Task<T> MakeHttpCall<T>(HttpMethod method, string url, Dictionary<string, string> headers, object request) where T : class
        {
            using (var httpRequest = new HttpRequestMessage(method, url))
            {
                foreach(KeyValuePair<string, string> header in headers)
                {
                    httpRequest.Headers.Add(header.Key, header.Value);
                }


                if (HttpMethod.Get != method || HttpMethod.Delete != method)
                {
                    httpRequest.Content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");
                }

                var response = await _httpClient.SendAsync(httpRequest);

                var resultBody = await response.Content.ReadAsStringAsync();

                if (response.IsSuccessStatusCode)
                {
                    _logger.LogDebug($"Received a success response for api call to {url}");
                    var result = JsonConvert.DeserializeObject<T>(resultBody);
                    return result;
                }
                else
                {
                    _logger.LogError($"Api call to {url} resulted in error {resultBody}");
                    throw new ApiException((int)response.StatusCode, resultBody);
                }              
            }
        }

        public void Dispose()
        {
            _httpClient.Dispose();
        }
    }
}
