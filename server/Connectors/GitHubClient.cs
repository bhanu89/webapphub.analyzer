﻿using Microsoft.Extensions.Options;
using server.Connectors.ConnectorModels;
using server.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace server.Connectors
{
    public class GitHubClient : IGitHubClient
    {
        private readonly IBaseHttpClient _baseHttpClient;
        private readonly Config _config;

        public GitHubClient(IBaseHttpClient baseHttpClient, IOptions<Config> config)
        {
            _baseHttpClient = baseHttpClient;
            _config = config.Value;
        }

        public async Task<GitHubOrganization> GetOrganization(string name)
        {

            var url = $"{_config.GitHubBaseUrl}/orgs/{name.ToLowerInvariant()}";
            var headers = GetStandardHeaders();
            var response = await _baseHttpClient.Get<GitHubOrganization>(url, headers);
            return response;
        }

        public async Task<IEnumerable<GitHubProject>> GetProjectsForOrganization(string orgName)
        {
            var url = $"{_config.GitHubBaseUrl}/orgs/{orgName.ToLowerInvariant()}/projects";
            var headers = GetStandardHeaders();
            var response = await _baseHttpClient.Get<IEnumerable<GitHubProject>>(url, headers);
            return response;
        }

        public async Task<List<GitHubProjectColumn>> GetColumnsForProject(int projectId)
        {
            var url = $"{_config.GitHubBaseUrl}/projects/{projectId}/columns";
            var headers = GetStandardHeaders();
            var response = await _baseHttpClient.Get<List<GitHubProjectColumn>>(url, headers);
            return response;
        }

        public async Task<int> GetIssueCountForProjectColumn(int projectColumnId)
        {
            var url = $"{_config.GitHubBaseUrl}/projects/columns/{projectColumnId}/cards";
            var headers = GetStandardHeaders();
            var response = await _baseHttpClient.Get<List<GitHubCard>>(url, headers);
            if (response == null) { return 0; }
            return response.Count;
        }

        public async Task<IEnumerable<GitHubRepository>> GetReposForOrganization(string orgName)
        {
            var url = $"{_config.GitHubBaseUrl}/orgs/{orgName.ToLowerInvariant()}/repos";
            var headers = GetStandardHeaders();
            var response = await _baseHttpClient.Get<IEnumerable<GitHubRepository>>(url, headers);
            return response;
        }

        public async Task<List<GitHubCommitWrapper>> GetLatestCommitsForRepo(string orgName, string repoName, string sinceDateTime)
        {
            var url = $"{_config.GitHubBaseUrl}/repos/{orgName.ToLowerInvariant()}/{repoName.ToLowerInvariant()}/commits?since={sinceDateTime}";
            var headers = GetStandardHeaders();
            var response = await _baseHttpClient.Get<List<GitHubCommitWrapper>>(url, headers);
            return response;
        }

        private Dictionary<string, string> GetStandardHeaders()
        {
            var headers = new Dictionary<string, string>
            {
                { "Authorization", $"token {_config.GitHubPrivateKey}" },
                { "Accept", _config.GitHubPreviewHeader },
                { "User-Agent", "bhanu-app" }
            };
            return headers;
        }


    }
}
