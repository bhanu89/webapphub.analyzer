﻿using server.Connectors.ConnectorModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace server.Connectors
{
    public interface IGitHubClient
    {

        Task<GitHubOrganization> GetOrganization(string name);
        Task<IEnumerable<GitHubProject>> GetProjectsForOrganization(string orgName);
        Task<List<GitHubProjectColumn>> GetColumnsForProject(int projectId);
        Task<int> GetIssueCountForProjectColumn(int projectColumnId);
        Task<IEnumerable<GitHubRepository>> GetReposForOrganization(string orgName);
        Task<List<GitHubCommitWrapper>> GetLatestCommitsForRepo(string orgName, string repoName, string sinceDateTime);
    }
}
