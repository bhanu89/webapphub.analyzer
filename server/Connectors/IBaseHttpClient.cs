﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace server.Connectors
{
    public interface IBaseHttpClient
    {

        Task<T> Get<T>(string url, Dictionary<string, string> headers) where T : class;

        Task<T> Post<T>(string url, Dictionary<string, string> headers, object request) where T : class;

        Task<T> Put<T>(string url, Dictionary<string, string> headers, object request) where T : class;

        Task<T> Delete<T>(string url, Dictionary<string, string> headers) where T : class;

    }
}