﻿using System;

namespace server.Connectors.ConnectorModels
{
    public class GitHubCard
    {
        public int Id { get; set; }
        public DateTime Created_At { get; set; }
        public DateTime Updated_At { get; set; }
    }
}
