﻿using System;

namespace server.Connectors.ConnectorModels
{
    public class GitHubRepository
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Full_Name { get; set; }
        public string Description { get; set; }
        public DateTime Created_At { get; set; }
        public DateTime Updated_At { get; set; }
        public int Open_Issues_Count { get; set; }

    }
}
