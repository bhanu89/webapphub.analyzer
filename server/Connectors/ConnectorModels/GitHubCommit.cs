﻿namespace server.Connectors.ConnectorModels
{
    public class GitHubCommit
    {
        public GitHubAuthor Author { get; set; }
        public string Message { get; set; }
    }
}
