﻿namespace server.Connectors.ConnectorModels
{
    public class GitHubOrganization
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Location { get; set; }
        public string Html_Url { get; set; }
        public string Repos_Url { get; set; }
        public string Blog { get; set; }
        public int Public_Repos { get; set; }
        public int Followers { get; set; }
        public string Description { get; set; }


    }
}
