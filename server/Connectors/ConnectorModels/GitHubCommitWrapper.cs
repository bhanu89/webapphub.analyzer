﻿namespace server.Connectors.ConnectorModels
{
    public class GitHubCommitWrapper
    {
        public string Sha { get; set; }
        public GitHubCommit Commit { get; set; }

    }
}
