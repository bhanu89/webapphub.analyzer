﻿using System;

namespace server.Connectors.ConnectorModels
{
    public class GitHubProject
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Body { get; set; }
        public string State { get; set; }
        public string Html_Url { get; set; }
        public string Columns_Url { get; set; }
        public DateTime Created_At { get; set; }
        public DateTime Updated_At { get; set; }
    }
}
