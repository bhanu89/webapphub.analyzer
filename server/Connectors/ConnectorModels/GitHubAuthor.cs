﻿using System;

namespace server.Connectors.ConnectorModels
{
    public class GitHubAuthor
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public DateTime Date { get; set; }
    }
}