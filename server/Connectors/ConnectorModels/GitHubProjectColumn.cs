﻿using System;

namespace server.Connectors.ConnectorModels
{
    public class GitHubProjectColumn
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Created_At { get; set; }
        public DateTime Updated_At { get; set; }
    }
}
