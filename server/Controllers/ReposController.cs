﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using server.Core;
using server.Exceptions;
using System.Threading.Tasks;

namespace server.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("v{version:apiVersion}/api/[Controller]")]
    public class ReposController : ControllerBase
    {

        private readonly IRepoRetriever _repoRetriever;
        private readonly ILogger _logger;

        public ReposController(IRepoRetriever repoRetriever, ILogger<ReposController> logger)
        {
            _repoRetriever = repoRetriever;
            _logger = logger;
        }

        // Get all latest commit counts for all of organization's repos
        [HttpGet]
        [Route("latest-commits")]
        public async Task<IActionResult> GetLatestCommitsForOrganization([FromQuery] string organizationName, [FromQuery] int daysPast)
        {
            try
            {
                var response = await _repoRetriever.GetLatestCommitsForOrganizationRepos(organizationName, daysPast);
                _logger.LogDebug($"Returning a success response for commit search for org {organizationName} for past {daysPast} days");
                return Ok(response);
            }
            catch (ApiException ex)
            {
                _logger.LogError($"Commit search for org {organizationName} for past {daysPast} days resulted in an error", ex);
                return new StatusCodeResult(ex.HttpStatusCode);
            }
        }
    }
}
