﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using server.Core;
using server.Exceptions;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace server.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("v{version:apiVersion}/api/[Controller]")]
    public class ProjectsController : ControllerBase
    {

        private readonly IProjectRetriever _projectRetriever;
        private readonly ILogger _logger;

        public ProjectsController(IProjectRetriever projectRetriever, ILogger<ProjectsController> logger)
        {
            _projectRetriever = projectRetriever;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> GetProjectsForOrganization([FromQuery] string organizationName)
        {
            try
            {
                var projects = await _projectRetriever.GetProjectsByOrgName(organizationName);
                _logger.LogDebug($"Returning successful response for project search for organization {organizationName}");
                return Ok(projects);
            }
            catch (ApiException ex)
            {
                _logger.LogError($"Project search for organization {organizationName} returned error response", ex);
                return new StatusCodeResult(ex.HttpStatusCode);
            }
        }

        [HttpGet]
        [Route("status/{projectId:int}")]
        public async Task<IActionResult> GetProjectStatus([FromRoute] int projectId)
        {
            try
            {
                var projectStatus = await _projectRetriever.GetProjectStatus(projectId);
                _logger.LogDebug($"Returning successful response for project status for project {projectId}");
                return Ok(projectStatus);
            }
            catch (ApiException ex)
            {
                _logger.LogError($"Status retrieval for project {projectId} restuled in error", ex);
                return new StatusCodeResult(ex.HttpStatusCode);
            }
        }

    }
}
