﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using server.Core;
using server.Exceptions;

namespace server.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("v{version:apiVersion}/api/[Controller]")]
    public class OrganizationsController : ControllerBase
    {
        private readonly IOrganizationRetriever _organizationRetriever;
        private readonly ILogger _logger;

        public OrganizationsController(IOrganizationRetriever organizationRetriever, ILogger<OrganizationsController> logger)
        {
            _organizationRetriever = organizationRetriever;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> GetOrganization([FromQuery] string name)
        {
            try
            {
                var organization = await _organizationRetriever.GetOrganizationByName(name);
                _logger.LogDebug("$Returning successful response for org search with name {name}");
                return Ok(organization);
            }
            catch (ApiException ex)
            {
                _logger.LogError($"Org search for name {name} returned error response", ex);
                return new StatusCodeResult(ex.HttpStatusCode);
            }
        }
    }
}