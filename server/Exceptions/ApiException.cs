﻿using System;

namespace server.Exceptions
{
    public class ApiException:Exception
    {
        public int HttpStatusCode { get; private set; }

        public ApiException(int statusCode, string message):base(message)
        {
            HttpStatusCode = statusCode;
        }
    }
}
