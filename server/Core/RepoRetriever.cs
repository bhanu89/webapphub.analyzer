﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using server.Connectors;
using server.Models;
using server.Models.v1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.Core
{
    public class RepoRetriever : IRepoRetriever
    {
        private readonly Config _config;
        private readonly IMemoryCache _memoryCache;
        private readonly IGitHubClient _gitHubClient;
        private readonly IGitHubModelMapper _gitHubModelMapper;
        private readonly ILogger _logger;

        private const string ORG_REPOS_CACHE_PREFIX = "OrgRepos";
        private const string DATETIME_FORMAT_FOR_GITHUB = "yyyy-MM-ddTHH:mm:ssZ";

        public RepoRetriever(IGitHubClient gitHubClient, IGitHubModelMapper gitHubModelMapper, 
            IMemoryCache memoryCache, IOptions<Config> config, ILogger<RepoRetriever> logger)
        {
            _gitHubClient = gitHubClient;
            _gitHubModelMapper = gitHubModelMapper;
            _memoryCache = memoryCache;
            _config = config.Value;
            _logger = logger;
        }

        public async Task<List<CommitsByAuthorWrapper>> GetLatestCommitsForOrganizationRepos(string orgName, int daysPast)
        {
            // get repos for organization
            _memoryCache.TryGetValue(GetOrgReposCacheKey(orgName), out IEnumerable<Repository> repos);
            if (repos == null)
            {
                var gitHubRepos = await _gitHubClient.GetReposForOrganization(orgName);
                repos = _gitHubModelMapper.MapGitHubRepositoryModelToDomainModel(gitHubRepos);
                if (repos != null)
                {
                    SaveReposToCache(repos, orgName);
                }
            }

            if (repos == null)
            {
                return null;
            }

            // iterate through repos and gather all latest commits
            var allCommits = new List<Commit>();
            var sinceDateTime = DateTime.UtcNow.AddDays(-daysPast).ToString(DATETIME_FORMAT_FOR_GITHUB);

            foreach (var repo in repos)
            {
                var gitHubCommits = await _gitHubClient.GetLatestCommitsForRepo(orgName, repo.Name, sinceDateTime);
                var domainCommits = _gitHubModelMapper.MapGitHubCommitsModelToDomainModel(gitHubCommits, repo.Name);
                allCommits.AddRange(domainCommits);
            }

            if (!allCommits.Any())
            {
                return null;
            }

            // group commits by author and then subgroup by repo
            var groupedCommits = allCommits.GroupBy(x => $"{x.AuthorName} [{x.AuthorEmail}]")
                .Select(c => new CommitsByAuthorWrapper
                {
                    Author = c.Key,
                    Commits = c.GroupBy(x => x.RepoName).ToDictionary(x => x.Key, x => x.ToList())
                }).ToList() ;

            // order by author name
            groupedCommits.Sort((x,y)=>x.Author.CompareTo(y.Author));

            return groupedCommits;
        }

        private void SaveReposToCache(IEnumerable<Repository> repos, string orgName)
        {
            var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromSeconds(_config.OrgReposCacheTimeInSeconds));
            _memoryCache.Set(GetOrgReposCacheKey(orgName), repos, cacheEntryOptions);
        }

        private object GetOrgReposCacheKey(string orgName)
        {
            return $"{ORG_REPOS_CACHE_PREFIX}-{orgName.ToLowerInvariant()}";
        }
    }
}
