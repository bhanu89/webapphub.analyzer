﻿using System.Collections.Generic;
using Mapster;
using server.Connectors.ConnectorModels;
using server.Models.v1;

namespace server.Core
{
    public class GitHubModelMapper : IGitHubModelMapper
    {
        private readonly TypeAdapterConfig _typeAdapterConfig;

        public GitHubModelMapper()
        {
            _typeAdapterConfig = new TypeAdapterConfig();
            LoadMappings();
        }

        private void LoadMappings()
        {
            LoadOrganizationMapping();
            LoadProjectMapping();
            LoadRepositoryMapping();
            LoadCommitMapping();
        }

        private void LoadOrganizationMapping()
        {
            _typeAdapterConfig.NewConfig<GitHubOrganization, Organization>()
                .Map(dest=>dest.Name, src=>src.Login)
                .Map(dest => dest.Url, src => src.Html_Url)
                .Map(dest => dest.BlogUrl, src => src.Blog)
                .Map(dest => dest.ReposUrl, src => src.Repos_Url)
                .Map(dest => dest.PublicRepoCount, src => src.Public_Repos)
                .Map(dest => dest.FollowerCount, src => src.Followers);
        }

        private void LoadProjectMapping()
        {
            _typeAdapterConfig.NewConfig<GitHubProject, Project>()
                .Map(dest => dest.Status, src => src.State)
                .Map(dest => dest.Description, src => src.Body)
                .Map(dest => dest.IssueStatusesUrl, src => src.Columns_Url)
                .Map(dest => dest.Created, src => src.Created_At)
                .Map(dest => dest.Modified, src => src.Updated_At);
        }

        private void LoadRepositoryMapping()
        {
            _typeAdapterConfig.NewConfig<GitHubRepository, Repository>()
                .Map(dest => dest.FullName, src => src.Full_Name)
                .Map(dest => dest.Created, src => src.Created_At)
                .Map(dest => dest.Modified, src => src.Updated_At)
                .Map(dest => dest.OpenIssueCount, src => src.Open_Issues_Count);
        }

        private void LoadCommitMapping()
        {
            _typeAdapterConfig.NewConfig<GitHubCommitWrapper, Commit>()
                .Map(dest => dest.Id, src => src.Sha)
                .Map(dest => dest.AuthorName, src => src.Commit.Author.Name)
                .Map(dest => dest.AuthorEmail, src => src.Commit.Author.Email)
                .Map(dest => dest.Created, src => src.Commit.Author.Date)
                .Map(dest=>dest.Message, src=>src.Commit.Message);
        }

        public Organization MapGitHubOrganizationModelToDomainModel(GitHubOrganization gitHubOrganization)
        {
            return gitHubOrganization.Adapt<Organization>(_typeAdapterConfig);
        }

        public IEnumerable<Project> MapGitHubProjectsModelToDomainModel(IEnumerable<GitHubProject> gitHubProjects)
        {
            return gitHubProjects.Adapt<IEnumerable<Project>>(_typeAdapterConfig);
        }

        public List<ProjectColumn> MapGitHubProjectColumnsModelToDomainModel(List<GitHubProjectColumn> gitHubColumns)
        {
            return gitHubColumns.Adapt<List<ProjectColumn>>(_typeAdapterConfig);
        }

        public IEnumerable<Repository> MapGitHubRepositoryModelToDomainModel(IEnumerable<GitHubRepository> gitHubRepositories)
        {
            return gitHubRepositories.Adapt<IEnumerable<Repository>>(_typeAdapterConfig);
        }

        public List<Commit> MapGitHubCommitsModelToDomainModel(List<GitHubCommitWrapper> gitHubCommits, string repoName)
        {
            var mappedResponseList = gitHubCommits.Adapt<List<Commit>>(_typeAdapterConfig);
            foreach(var mappedResponse in mappedResponseList)
            {
                mappedResponse.RepoName = repoName;
            }

            return mappedResponseList;
        }
    }
}
