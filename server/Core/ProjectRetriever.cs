﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using server.Connectors;
using server.Models;
using server.Models.v1;

namespace server.Core
{
    public class ProjectRetriever : IProjectRetriever
    {
        private readonly Config _config;
        private readonly IMemoryCache _memoryCache;
        private readonly IGitHubClient _gitHubClient;
        private readonly IGitHubModelMapper _gitHubModelMapper;
        private readonly ILogger _logger;

        private const string PROJECTS_CACHE_PREFIX = "Projects";
        private const string PROJECT_COLUMNS_CACHE_PREFIX = "ProjectColumns";

        public ProjectRetriever(IGitHubClient gitHubClient, IGitHubModelMapper gitHubModelMapper,
            IMemoryCache memoryCache, IOptions<Config> config, ILogger<ProjectRetriever> logger)
        {
            _gitHubClient = gitHubClient;
            _gitHubModelMapper = gitHubModelMapper;
            _memoryCache = memoryCache;
            _config = config.Value;
            _logger = logger;
        }

        public async Task<IEnumerable<Project>> GetProjectsByOrgName(string organizationName)
        {
            _memoryCache.TryGetValue(GetProjectsCacheKey(organizationName), out IEnumerable<Project> cachedResponse);
            if (cachedResponse == null)
            {
                var gitHubResponse = await _gitHubClient.GetProjectsForOrganization(organizationName);
                var domainResponse = _gitHubModelMapper.MapGitHubProjectsModelToDomainModel(gitHubResponse);
                if (domainResponse != null)
                {
                    SaveProjectsToCache(domainResponse, organizationName);
                }
                return domainResponse;
            }
            else
            {
                _logger.LogDebug("Retrieved cached organization projects entry");
                return cachedResponse;
            }
        }

        private void SaveProjectsToCache(IEnumerable<Project> projects, string organizationName)
        {
            var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromSeconds(_config.OrgProjectsCacheTimeInSeconds));
            _memoryCache.Set(GetProjectsCacheKey(organizationName), projects, cacheEntryOptions);
        }

        private string GetProjectsCacheKey(string organizationName)
        {
            return $"{PROJECTS_CACHE_PREFIX}-{organizationName.ToLowerInvariant()}";
        }

        public async Task<ProjectStatus> GetProjectStatus(int projectId)
        {
            // get project columns
            _memoryCache.TryGetValue(GetProjectColumnsCacheKey(projectId), out List<ProjectColumn> projectColumns);
            if (projectColumns == null)
            {
                var gitHubResponse = await _gitHubClient.GetColumnsForProject(projectId);
                var domainResponse = _gitHubModelMapper.MapGitHubProjectColumnsModelToDomainModel(gitHubResponse);
                if (domainResponse != null)
                {
                    SaveProjectColumnsToCache(domainResponse, projectId);
                }
                projectColumns = domainResponse;
            }

            if (projectColumns == null)
            {
                return null;
            }

            var projectStatusResponse = new ProjectStatus() { IssueGroups = new List<ProjectIssueGroup>() };

            int totalNumberOfIssues = 0;

            // iterate through columns and get issue count per column
            foreach(var projectColumn in projectColumns)
            {
                int countOfIssuesInColumn = await _gitHubClient.GetIssueCountForProjectColumn(projectColumn.Id);
                totalNumberOfIssues += countOfIssuesInColumn;
                projectStatusResponse.IssueGroups.Add(new ProjectIssueGroup()
                {
                    Name = projectColumn.Name.ToUpperInvariant(),
                    IssueCount = countOfIssuesInColumn
                });
            }

            if (totalNumberOfIssues != 0)
            {
                foreach (var projectIssueGroup in projectStatusResponse.IssueGroups)
                {
                    projectIssueGroup.Weightage = Math.Round((double)projectIssueGroup.IssueCount / totalNumberOfIssues * 100, 2);
                }
            }

            return projectStatusResponse;
        }

        private string GetProjectColumnsCacheKey(int projectId)
        {
            return $"{PROJECT_COLUMNS_CACHE_PREFIX}-{projectId}";
        }

        private void SaveProjectColumnsToCache(List<ProjectColumn> projectColumns, int projectId)
        {
            var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromSeconds(_config.OrgProjectColumnsCacheTimeInSeconds));
            _memoryCache.Set(GetProjectColumnsCacheKey(projectId), projectColumns, cacheEntryOptions);
        }
    }
}
