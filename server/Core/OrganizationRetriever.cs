﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using server.Connectors;
using server.Models;
using server.Models.v1;

namespace server.Core
{
    public class OrganizationRetriever : IOrganizationRetriever
    {
        private readonly Config _config;
        private readonly IMemoryCache _memoryCache;
        private readonly IGitHubClient _gitHubClient;
        private readonly IGitHubModelMapper _gitHubModelMapper;
        private readonly ILogger _logger;

        private const string ORGANIZATIONS_CACHE_PREFIX = "Organization";

        public OrganizationRetriever(IGitHubClient gitHubClient, IGitHubModelMapper gitHubModelMapper, 
            IMemoryCache memoryCache, IOptions<Config> config, ILogger<OrganizationRetriever> logger)
        {
            _gitHubClient = gitHubClient;
            _gitHubModelMapper = gitHubModelMapper;
            _memoryCache = memoryCache;
            _config = config.Value;
            _logger = logger;
        }

        public async Task<Organization> GetOrganizationByName(string organizationName)
        {
            _memoryCache.TryGetValue(GetCacheKey(organizationName), out Organization cachedResponse);
            if (cachedResponse == null)
            {
                var gitHubResponse = await _gitHubClient.GetOrganization(organizationName);
                var domainResponse = _gitHubModelMapper.MapGitHubOrganizationModelToDomainModel(gitHubResponse);
                if (domainResponse != null) {
                    SaveToCache(domainResponse);
                }
                return domainResponse;
            }
            else
            {
                _logger.LogDebug("Retrieved cached orgnanization entry");
                return cachedResponse;
            }
        }

        private void SaveToCache(Organization domainResponse)
        {
            var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromSeconds(_config.OrganizationCacheTimeInSeconds));
            _memoryCache.Set(GetCacheKey(domainResponse.Name), domainResponse, cacheEntryOptions);
        }

        private string GetCacheKey(string organizationName)
        {
            return $"{ORGANIZATIONS_CACHE_PREFIX}-{organizationName.ToLowerInvariant()}";
        }
    }
}
