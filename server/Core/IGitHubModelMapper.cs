﻿using server.Connectors.ConnectorModels;
using server.Models.v1;
using System.Collections.Generic;

namespace server.Core
{
    public interface IGitHubModelMapper
    {
        Organization MapGitHubOrganizationModelToDomainModel(GitHubOrganization gitHubOrganization);
        IEnumerable<Project> MapGitHubProjectsModelToDomainModel(IEnumerable<GitHubProject> gitHubProjects);
        List<ProjectColumn> MapGitHubProjectColumnsModelToDomainModel(List<GitHubProjectColumn> gitHubColumns);
        IEnumerable<Repository> MapGitHubRepositoryModelToDomainModel(IEnumerable<GitHubRepository> gitHubRepositories);
        List<Commit> MapGitHubCommitsModelToDomainModel(List<GitHubCommitWrapper> gitHubCommits, string repoName);
    }
}
