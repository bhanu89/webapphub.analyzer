﻿using server.Models.v1;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace server.Core
{
    public interface IRepoRetriever
    {
        Task<List<CommitsByAuthorWrapper>> GetLatestCommitsForOrganizationRepos(string orgName, int daysPast);
    }
}
