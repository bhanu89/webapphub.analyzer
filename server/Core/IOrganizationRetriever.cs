﻿using server.Models.v1;
using System.Threading.Tasks;

namespace server.Core
{
    public interface IOrganizationRetriever
    {
        Task<Organization> GetOrganizationByName(string organizationName);
    }
}
