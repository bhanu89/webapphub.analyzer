﻿using server.Models.v1;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace server.Core
{
    public interface IProjectRetriever
    {
        Task<IEnumerable<Project>> GetProjectsByOrgName(string organizationName);
        Task<ProjectStatus> GetProjectStatus(int projectId);
    }
}
