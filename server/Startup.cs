﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using server.Connectors;
using server.Core;
using server.Models;

namespace server
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc((options)=> 
            {
                options.Filters.Clear();
                options.OutputFormatters.RemoveType(typeof(StringOutputFormatter));
                options.EnableEndpointRouting = true;
            })
            .SetCompatibilityVersion(CompatibilityVersion.Latest);
            services.AddCors();
            services.AddLogging();
            services.AddApiVersioning(options => { options.ReportApiVersions = true; });
            services.AddResponseCaching();
            services.AddResponseCompression();
            services.AddVersionedApiExplorer(options => 
            {
                options.GroupNameFormat = "'v'VVV";
                options.SubstituteApiVersionInUrl = true;
            });
            services.AddSwaggerGen(options =>
            {
                var provider = services.BuildServiceProvider().GetRequiredService<IApiVersionDescriptionProvider>();
                foreach(var description in provider.ApiVersionDescriptions)
                {
                    options.SwaggerDoc(
                        description.GroupName,
                        new Swashbuckle.AspNetCore.Swagger.Info()
                        {
                            Title = $"GitHub Project Analyzer API {description.ApiVersion}",
                            Version = description.ApiVersion.ToString()
                        });
                }
                options.CustomSchemaIds((type)=> { return type.FullName; });
            });

            services.AddHttpClient();
            services.AddMemoryCache();

            services.Configure<Config>(Configuration.GetSection("Config"));

            // singletons
            services.AddSingleton<IBaseHttpClient, BaseHttpClient>();
            services.AddSingleton<IGitHubClient, GitHubClient>();

            // transients
            services.AddTransient<IOrganizationRetriever, OrganizationRetriever>();
            services.AddTransient<IProjectRetriever, ProjectRetriever>();
            services.AddTransient<IRepoRetriever, RepoRetriever>();
            services.AddTransient<IGitHubModelMapper, GitHubModelMapper>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApiVersionDescriptionProvider provider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCors(policy => policy
            .AllowAnyHeader()
            .AllowAnyMethod()
            .WithOrigins("http://localhost:8080")
            .AllowCredentials());

            app.UseHttpsRedirection();
            app.UseMvc();
            app.UseSwagger();
            app.UseSwaggerUI(
                options => {
                    foreach(var description in provider.ApiVersionDescriptions)
                    {
                        options.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json", description.GroupName.ToUpperInvariant());
                    }
                });
        }
    }
}
