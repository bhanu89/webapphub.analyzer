﻿namespace server.Models
{
    public class Config
    {
        public string GitHubBaseUrl { get; set; }
        public string GitHubPrivateKey { get; set; }
        public string GitHubPreviewHeader { get; set; }
        public int OrganizationCacheTimeInSeconds { get; set; }
        public int OrgProjectsCacheTimeInSeconds { get; set; }
        public int OrgProjectColumnsCacheTimeInSeconds { get; set; }
        public int OrgReposCacheTimeInSeconds { get; set; }
    }
}
