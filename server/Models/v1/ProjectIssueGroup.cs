﻿namespace server.Models.v1
{
    public class ProjectIssueGroup
    {
        public string Name { get; set; }
        public int IssueCount { get; set; }
        public double Weightage { get; set; }
    }
}