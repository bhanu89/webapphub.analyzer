﻿using System;

namespace server.Models.v1
{
    public class Repository
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public int OpenIssueCount { get; set; }
    }
}
