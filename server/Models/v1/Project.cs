﻿using System;

namespace server.Models.v1
{
    public class Project
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string IssueStatusesUrl { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}
