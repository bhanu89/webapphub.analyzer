﻿using System.Collections.Generic;

namespace server.Models.v1
{
    public class CommitsByAuthorWrapper
    {
        public string Author { get; set; }
        public Dictionary<string, List<Commit>> Commits { get; set; }
    }
}
