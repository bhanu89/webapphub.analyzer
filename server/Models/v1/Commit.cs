﻿using System;

namespace server.Models.v1
{
    public class Commit
    {
        public string Id { get; set; }
        public string RepoName { get; set; }
        public string Message { get; set; }
        public string AuthorName { get; set; }
        public string AuthorEmail { get; set; }
        public DateTime Created { get; set; }
    }
}
