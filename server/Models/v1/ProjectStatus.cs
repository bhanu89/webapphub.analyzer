﻿using System.Collections.Generic;

namespace server.Models.v1
{
    public class ProjectStatus
    {
        public List<ProjectIssueGroup> IssueGroups { get; set; }
    }
}
