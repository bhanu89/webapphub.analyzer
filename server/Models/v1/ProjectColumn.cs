﻿namespace server.Models.v1
{
    public class ProjectColumn
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
