﻿namespace server.Models.v1
{
    public class Organization
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public string Url { get; set; }
        public string ReposUrl { get; set; }
        public string BlogUrl { get; set; }
        public int PublicRepoCount { get; set; }
        public int FollowerCount { get; set; }
        public string Description { get; set; }

    }
}
