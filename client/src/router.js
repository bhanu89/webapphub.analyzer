import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';

Vue.use(Router);

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/organization',
            name: 'organization',
            component: () => import('./views/Organization.vue')
        },
        {
            path: '/orgcommits',
            name: 'orgcommits',
            component: () => import('./views/OrgCommits.vue')
        }
    ]
});
