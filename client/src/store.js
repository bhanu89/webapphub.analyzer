import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import createPersistedState from 'vuex-persistedstate';

Vue.use(Vuex);

function getDefaultState() {
    return {
        orgName: '',
        apiUrl: 'https://localhost:44312',
        organization: [],
        projects: [],
        projectStatus: [],
        orgCommits: []
    };
}

export default new Vuex.Store({
    plugins: [createPersistedState()],
    state: getDefaultState,
    mutations: {
        setOrgName(state, payload) {
            state.orgName = payload;
        },
        setApiUrl(state, payload) {
            state.apiUrl = payload;
        },
        setOrganization(state, payload) {
            state.organization = payload;
        },
        setProjects(state, payload) {
            state.projects = payload;
        },
        setProjectStatus(state, payload) {
            state.projectStatus = payload;
        },
        setOrgCommits(state, payload) {
            state.orgCommits = payload;
        },
        resetState(state) {
            const s = getDefaultState();
            Object.keys(s).forEach(key => {
                state[key] = s[key];
            });
        }
    },
    actions: {
        async getOrganization({ state, commit }, orgName) {
            try {
                let response = await axios.get(
                    `${state.apiUrl}/v1/api/Organizations`,
                    {
                        params: {
                            name: orgName
                        }
                    }
                );
                commit('setOrganization', response.data);
            } catch (error) {
                commit('resetState');
            }
        },

        async getOrganizationCommits({ state, commit }, orgName) {
            try {
                let response = await axios.get(
                    `${state.apiUrl}/v1/api/Repos/latest-commits`,
                    {
                        params: {
                            organizationName: orgName,
                            daysPast: 30
                        }
                    }
                );
                commit('setOrgCommits', response.data);
            } catch (error) {
                commit('resetState');
            }
        }
    }
});
