﻿using AutoFixture;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using server.Connectors;
using server.Connectors.ConnectorModels;
using server.Core;
using server.Models;
using server.Models.v1;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace server.test.Core
{
    public class ProjectRetrieverTests
    {
        private readonly Fixture _fixture;
        private readonly ProjectRetriever _projectRetriever;

        private readonly IOptions<Config> _config;
        private readonly IMemoryCache _memoryCache;
        private readonly Mock<IGitHubClient> _gitHubClient;
        private readonly Mock<IGitHubModelMapper> _gitHubModelMapper;
        private readonly Mock<ILogger<ProjectRetriever>> _logger;

        public ProjectRetrieverTests()
        {
            _fixture = new Fixture();

            var cacheOptions = new Mock<IOptions<MemoryCacheOptions>>();
            cacheOptions.Setup(x => x.Value).Returns(new MemoryCacheOptions());
            _memoryCache = new MemoryCache(cacheOptions.Object);

            _config = Options.Create<Config>(new Config
            {
                OrgProjectsCacheTimeInSeconds = 100,
                OrgProjectColumnsCacheTimeInSeconds = 100
            });
            _gitHubClient = new Mock<IGitHubClient>();
            _gitHubModelMapper = new Mock<IGitHubModelMapper>();
            _logger = new Mock<ILogger<ProjectRetriever>>();

            _projectRetriever = new ProjectRetriever(_gitHubClient.Object, _gitHubModelMapper.Object,
                _memoryCache, _config, _logger.Object);
        }

        [Fact]
        public async Task ProjectRetriever_GetProjectsByOrgName_HappyPath_GivesValidResponse()
        {
            var connectorResponse = _fixture.Create<List<GitHubProject>>();
            _gitHubClient.Setup(x => x.GetProjectsForOrganization(It.IsAny<string>())).ReturnsAsync(connectorResponse);
            _gitHubModelMapper.Setup(x => x.MapGitHubProjectsModelToDomainModel(It.IsAny<List<GitHubProject>>())).Returns(_fixture.Create<List<Project>>());

            var response = await _projectRetriever.GetProjectsByOrgName("test");

            Assert.NotNull(response);
            Assert.Equal(response.ToList().Count, connectorResponse.ToList().Count);
        }

        [Fact]
        public async Task ProjectRetriever_GetProjectStatus_HappyPath_GivesValidResponse()
        {
            var connectorResponse = _fixture.Create<List<GitHubProjectColumn>>();
            _gitHubClient.Setup(x => x.GetColumnsForProject(It.IsAny<int>())).ReturnsAsync(connectorResponse);
            _gitHubModelMapper.Setup(x => x.MapGitHubProjectColumnsModelToDomainModel(It.IsAny<List<GitHubProjectColumn>>())).Returns(_fixture.Create<List<ProjectColumn>>());
            _gitHubClient.Setup(x => x.GetIssueCountForProjectColumn(It.IsAny<int>())).ReturnsAsync(3);

            var response = await _projectRetriever.GetProjectStatus(1);

            Assert.NotNull(response);
            Assert.Equal(response.IssueGroups.Count, connectorResponse.Count);
            Assert.True(response.IssueGroups.First().IssueCount==3);
        }

    }
}
