﻿using AutoFixture;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using server.Connectors;
using server.Connectors.ConnectorModels;
using server.Core;
using server.Models;
using server.Models.v1;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace server.test.Core
{
    public class RepoRetrieverTests
    {
        private readonly Fixture _fixture;
        private readonly RepoRetriever _repoRetriever;

        private readonly IOptions<Config> _config;
        private readonly IMemoryCache _memoryCache;
        private readonly Mock<IGitHubClient> _gitHubClient;
        private readonly Mock<IGitHubModelMapper> _gitHubModelMapper;
        private readonly Mock<ILogger<RepoRetriever>> _logger;

        public RepoRetrieverTests()
        {
            _fixture = new Fixture();

            var cacheOptions = new Mock<IOptions<MemoryCacheOptions>>();
            cacheOptions.Setup(x => x.Value).Returns(new MemoryCacheOptions());
            _memoryCache = new MemoryCache(cacheOptions.Object);

            _config = Options.Create<Config>(new Config
            {
                OrgReposCacheTimeInSeconds = 100
            });
            _gitHubClient = new Mock<IGitHubClient>();
            _gitHubModelMapper = new Mock<IGitHubModelMapper>();
            _logger = new Mock<ILogger<RepoRetriever>>();

            _repoRetriever = new RepoRetriever(_gitHubClient.Object, _gitHubModelMapper.Object,
                _memoryCache, _config, _logger.Object);
        }

        [Fact]
        public async Task RepoRetriever_GetLatestCommitsForOrganizationRepos_HappyPath_ReturnsGroupedCommits()
        {
            var connectorRepoResponse = _fixture.Create<List<GitHubRepository>>();
            var connectorCommitResponse = _fixture.Create<List<GitHubCommitWrapper>>();
            _gitHubClient.Setup(x => x.GetReposForOrganization(It.IsAny<string>())).ReturnsAsync(connectorRepoResponse);
            _gitHubClient.Setup(x => x.GetLatestCommitsForRepo(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(connectorCommitResponse);
            _gitHubModelMapper.Setup(x => x.MapGitHubRepositoryModelToDomainModel(It.IsAny<List<GitHubRepository>>()))
                .Returns(_fixture.Create<List<Repository>>());
            _gitHubModelMapper.Setup(x => x.MapGitHubCommitsModelToDomainModel(It.IsAny<List<GitHubCommitWrapper>>(), It.IsAny<string>()))
                .Returns(_fixture.Create<List<Commit>>());

            var response = await _repoRetriever.GetLatestCommitsForOrganizationRepos("test", 10);

            Assert.NotNull(response);
            // total commits before and after grouping should match
            Assert.True(response.Count * response.First().Commits.Values.First().Count == connectorRepoResponse.Count * connectorCommitResponse.Count);
        }
    }
}
