﻿using AutoFixture;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using server.Connectors;
using server.Connectors.ConnectorModels;
using server.Core;
using server.Models;
using server.Models.v1;
using System.Threading.Tasks;
using Xunit;

namespace server.test.Core
{
    public class OrganizationRetrieverTests
    {

        private readonly Fixture _fixture;
        private readonly OrganizationRetriever _organizationRetriever;

        private readonly IOptions<Config> _config;
        private readonly IMemoryCache _memoryCache;
        private readonly Mock<IGitHubClient> _gitHubClient;
        private readonly Mock<IGitHubModelMapper> _gitHubModelMapper;
        private readonly Mock<ILogger<OrganizationRetriever>> _logger;

        public OrganizationRetrieverTests()
        {
            _fixture = new Fixture();

            var cacheOptions = new Mock<IOptions<MemoryCacheOptions>>();
            cacheOptions.Setup(x => x.Value).Returns(new MemoryCacheOptions());
            _memoryCache = new MemoryCache(cacheOptions.Object);

            _config = Options.Create<Config>(new Config
            {
                OrganizationCacheTimeInSeconds = 100
            });
            _gitHubClient = new Mock<IGitHubClient>();
            _gitHubModelMapper = new Mock<IGitHubModelMapper>();
            _logger = new Mock<ILogger<OrganizationRetriever>>();

            _organizationRetriever = new OrganizationRetriever(_gitHubClient.Object, _gitHubModelMapper.Object, 
                _memoryCache, _config, _logger.Object);
        }

        [Fact]
        public async Task OrganizationRetriever_GetOrganizationByName_HappyPath_ReturnsAResponse()
        {
            _gitHubClient.Setup(x => x.GetOrganization(It.IsAny<string>())).ReturnsAsync(_fixture.Create<GitHubOrganization>());
            _gitHubModelMapper.Setup(x => x.MapGitHubOrganizationModelToDomainModel(It.IsAny<GitHubOrganization>())).Returns(_fixture.Create<Organization>());

            var response = await _organizationRetriever.GetOrganizationByName("test");

            Assert.NotNull(response);
            _gitHubClient.Verify(x => x.GetOrganization(It.IsAny<string>()), Times.Once);
        }
    }
}
